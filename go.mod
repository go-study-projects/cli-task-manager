module gitlab.com/go-study-projects/task

go 1.21.1

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/cobra v1.7.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.13.0 // indirect
)
