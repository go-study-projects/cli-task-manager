package main

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/go-study-projects/task/cmd"
	"gitlab.com/go-study-projects/task/db"
)

func main() {
	homeDir, _ := os.UserHomeDir()
	dbPath := filepath.Join(homeDir, "tasks.db")
	must(db.Init(dbPath))

	cmd.Execute()
}

func must(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
