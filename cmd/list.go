package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/go-study-projects/task/db"
)

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List tasks.",
	Run: func(cmd *cobra.Command, args []string) {
		tasks, err := db.GetAllTasks()
		if err != nil {
			fmt.Println("Something went wrong:", err)
		}

		if len(tasks) == 0 {
			fmt.Println("Task list is empty.")
			return
		}
		for i, task := range tasks {
			fmt.Println(i+1, task.Value)
		}
	},
}

func init() {
	rootCmd.AddCommand(listCmd)
}
