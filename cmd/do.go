package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/go-study-projects/task/db"
)

var doCmd = &cobra.Command{
	Use:   "do",
	Short: "Mark task as done.",
	Run: func(cmd *cobra.Command, args []string) {
		var ids []int
		for _, arg := range args {
			id, err := strconv.Atoi(arg)
			if err != nil {
				fmt.Println("Failed to parse the argument:", arg)
				continue
			}
			ids = append(ids, id)
		}

		tasks, err := db.GetAllTasks()
		if err != nil {
			fmt.Println("Something went wrong:", err)
		}

		for _, id := range ids {
			if id <= 0 || id > len(tasks) {
				fmt.Println("Invalid id:", id)
				continue
			}
			task := tasks[id-1]
			if err := db.DeleteTask(task.Key); err != nil {
				fmt.Println("Something went wrong:", err)
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(doCmd)
}
