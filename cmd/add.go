package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/go-study-projects/task/db"
)

var addCmd = &cobra.Command{
	Use:   "add",
	Short: "Add a task.",
	Run: func(cmd *cobra.Command, args []string) {
		task := strings.Join(args, " ")
		if _, err := db.CreateTask(task); err != nil {
			fmt.Println("Something went wrong:", err)
			return
		}
		fmt.Printf("Added '%s' to your task list.\n", task)
	},
}

func init() {
	rootCmd.AddCommand(addCmd)
}
